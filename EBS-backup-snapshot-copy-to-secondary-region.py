import boto.ec2

source_region
destination_region

conn = boto.ec2.connect_to_region('us-west-2')
conn.copy_snapshot(source_region='us-east-1', source_snap_id='snap-12345678',
                   description='My new copy')
