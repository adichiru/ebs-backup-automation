# Automated EBS backup solution
Some long time ago there was the EBS Snapshot scheduler; it got replaced recently by AWS Ops Automator.

This solution however is too complex and complicated (for example, it involves DynamoDB databases) and it covers more than EBS.

The followings describe a simpler, more elegant solution to automate the backup of EBS volumes via snapshots and to manage the lifecycle of these snapshots.

For more details on the AWS Python SDK (Boto), see here: https://boto3.readthedocs.io/en/latest/

EBS is part of the EC2 API:https://boto3.readthedocs.io/en/latest/reference/services/ec2.html

## Goals

- [x] Automatically take a snapshot of each EBS volume in use according to a time/date schedule.
- [ ] Automatically copy this snapshot to a secondary region (e.g DR region).
- [x] Automatically clean up older snapshots (keep only the last *n* snapshots).

## Requirements
- IAM role with proper permissions
- RTO/RPO requirements for configuration of backup frequency and lifecycle.

## Logic
- CloudWatch Events Rule based on date/time -> triggers Lambda function to initiate the creation of snapshots for all EBS volumes in use.
- CloudWatch Events Rule based on date/time -> triggers Lambda function to delete EBS snapshots older than *n* days.


## Implementation

1. Create the IAM role so that Lambda functions can operate on EBS volumes
   - In the AWS Console, under Services find IAM.
   - Go to Roles -> Create role -> AWS Service -> Lambda
     - Permissions – create a new policy:
       - use the contents of the **Allow-Lambda-backup-EBS.json** file.
       - Name: **Allow-Lambda-backup-EBS**
       - Description: *This policy allows Lambda to work with EBS and EBS snapshots in order to automate the backup operations for EBS volumes.*
     - Role name: **EBS-backup-via-Lambda-Role**
     - Role description: *Allows Lambda functions to work with EBS volumes and snapshots in order to automate the backup operations for EBS volumes.*

2. Create the Lambda function to take snapshots
   - In the AWS Console, under Services find Lambda.
   - Create function:
     - Author from Scratch
     - Name: **EBS-backup-snapshot-creation**
     - Runtime: Python2.7
     - Role -> chose existing role: **EBS-backup-via-Lambda-Role**
     - Function code -> use the contents of the **EBS-backup-snapshot-creation.py** file.
     - Basic settings -> Timeout: 1 min

3. Create the Lambda function to copy the snapshots
   - In the AWS Console, under Services find Lambda.
   - Create function:
     - Author from Scratch
     - Name: **EBS-backup-snapshot-copy-to-secondary-region**
     - Runtime: Python2.7
     - Role -> chose existing role: **EBS-backup-via-Lambda-Role**
     - Function code -> use the contents of the **EBS-backup-snapshot-copy-to-secondary-region.py** file.

4. Create the Lambda function to delete old snapshots
   - In the AWS Console, under Services find Lambda.
   - Create function:
     - Author from Scratch
     - Name: **EBS-backup-snapshot-deletion**
     - Runtime: Python2.7
     - Role -> chose existing role: **EBS-backup-via-Lambda-Role**
     - Function code -> use the contents of the **EBS-backup-snapshot-deletion.py** file.
     - Basic settings -> Timeout: 1 min

5. Create the CloudWatch Events Rule to trigger the snapshots creation
   - In the AWS Console, under Services find CloudWatch.
   - Rules -> Create rule:
     - Schedule -> Cron expression: 0 12 * * ? *
     - Targets: Lambda function: **EBS-backup-snapshot-creation**
     - Name: **Create-EBS-backup-snapshot-prod**
     - Description: *This rule is triggering a Lambda function to create a snapshot for an EBS volume.*
     - State: Enabled

6. Create the CloudWatch Events Rule to trigger the old snapshots deletion
   - In the AWS Console, under Services find CloudWatch.
   - Rules -> Create rule:
     - Schedule -> Cron expression: 0 14 * * ? *
     - Targets: Lambda function: **EBS-backup-snapshot-deletion**
     - Name: **Delete-EBS-backup-snapshot-prod**
     - Description: *This rule is triggering a Lambda function to delete older EBS snapshots.*
     - State: Enabled

## Testing Lambda functions

To test the above functions simply run a test (Test button) with no parameters (yes, the best function is a function that needs no parameters :) ). This functionality sometimes is not very stable; I've seen error reports, after a previous error, that were not real, so use with care and patience and refresh everything if needed.

## Possible future improvements
- extract parameters/variables from python scripts to Lambda tags
- add notifications/alerting
