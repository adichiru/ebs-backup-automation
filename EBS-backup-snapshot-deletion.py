#!/usr/bin/env python

# python2.7 lambda script

# To do:
# - add better error handling

import boto3
from botocore.exceptions import ClientError
from datetime import datetime,timedelta

# Variables:
# AWS Account ID
account_id = '???'
# Snapshot retention period (days)
retention_period = 7
# Main/primary AWS region
primary_region = 'us-west2'
# DR/secondary AWS region
secondary_region = 'us-east1'

def delete_snapshot(snapshot_id):
    print "Deleting snapshot %s." % (snapshot_id)
    try:
        resource_client = boto3.resource('ec2')
        snapshot = resource_client.Snapshot(snapshot_id)
        snapshot.delete()
    except ClientError as e:
        print "Caught exception: %s" % e
    return

def lambda_handler(event, context):
    now = datetime.now()
    ec2_client = boto3.client('ec2')
    # Getting the IDs of every snapshot:
    result = ec2_client.describe_snapshots(OwnerIds=[account_id])
    # Looping through all snapshots to find the ones to delete:
    for snapshot in result['Snapshots']:
        print "Verify snapshot %s (created on %s)." % (snapshot['SnapshotId'],snapshot['StartTime'])
        # the timezone is breaking the time comparison; remove it:
        snapshot_time = snapshot['StartTime'].replace(tzinfo=None)
        # Compare timedelta with retention_period
        if (now - snapshot_time) > timedelta(retention_period):
            print "Snapshot %s is older than configured retention period (%d days). Deleting it..." % (snapshot, retention_period)
            delete_snapshot(snapshot['SnapshotId'])
        else:
            print "Skipping this snapshot (newer than configured retention period of %d days)." % (retention_period)
