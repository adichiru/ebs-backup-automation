#!/usr/bin/env python

# python2.7 lambda script

# To do:
# - copy to the DR region - for now the copy operation is designed to consider the current region as the destionation
# region so we can't copy the snapshots by running this lambda function in the primary region.
# - add better error handling

import boto3
from botocore.exceptions import ClientError

# Variables:
# Main/primary AWS region
primary_region = 'us-west2'
# DR/secondary AWS region
secondary_region = 'us-east1'

def lambda_handler(event, context):
    ec2_client = boto3.client('ec2')

    # Creating a list with all EBS Volumes that are in use:
    result = ec2_client.describe_volumes(Filters=[{'Name': 'status', 'Values': ['in-use']}])
    # looping through the list to trigger the creation of a snapshot for each EBS Volume:
    for volume in result['Volumes']:
        print "Starting backup snapshot for %s." % (volume['VolumeId'])
        result = ec2_client.create_snapshot(VolumeId=volume['VolumeId'],Description='Created by Lambda for backup.')

        # To identify easier the Volume of each snapshot, add a tag to the snapshot with the name of the Volume:
        if 'Tags' in volume:
            for tags in volume['Tags']:
                if tags["Key"] == 'Name':
                    volume_name = tags["Value"]
        else:
            volume_name = 'Not available'
        try:
            snapshot.create_tags(Tags=[{'Key': 'Source Volume Name','Value': volume_name}])
        except ClientError as e:
            print "Caught exception: %s" % e

        # Copy the snapshot to the secondary region:
        #snapshot.copy_snapshot(source_region = primary_region,
        #                       source_snap_id = snapshot,
        #                       description = 'Created by Lambda for backup.')
